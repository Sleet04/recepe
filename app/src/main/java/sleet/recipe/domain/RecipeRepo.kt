package sleet.recipe.domain

import sleet.recipe.providers.recipe.toRecipeEntity
import javax.inject.Inject

class RecipeRepo @Inject constructor(val database: AppDatabase) {

	fun save(model: Recipe){
		database.recipeDao().insert(model.toRecipeEntity())
		println(database.recipeDao().getAll())
	}
}