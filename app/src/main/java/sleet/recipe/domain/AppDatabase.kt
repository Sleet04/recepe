package sleet.recipe.domain

import androidx.room.Database
import androidx.room.RoomDatabase
import sleet.recipe.providers.recipe.RecipeDao
import sleet.recipe.providers.recipe.RecipeEntity

@Database(entities = [RecipeEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao

}
