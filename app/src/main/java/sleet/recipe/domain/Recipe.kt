package sleet.recipe.domain

data class Recipe(
    var name:String = "",
    val ingredients: MutableMap<String, String> = mutableMapOf(),
    var instructions: String = "",
    var dishType: DishType = DishType.PLAT,
    var difficulty: Difficulty = Difficulty.UNKNOWN,
    var diet: Diet = Diet.VEGETARIEN,
    var timeNeeded: Int = 0,
    val tags: MutableList<String> = arrayListOf()
) {
    enum class DishType{
        DESSERT, PLAT, ENTREE, APERITIF, BOISSON
    }
    enum class Difficulty{
        FACILE, MOYEN, DIFFICILE, UNKNOWN
    }
    enum class Diet{
        OMNI, VEGETARIEN, VEGAN
    }
}