package sleet.recipe.providers.recipe

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import sleet.recipe.domain.Recipe

@Entity(tableName = "recipe")
data class RecipeEntity (
    @PrimaryKey(autoGenerate = false)
    var id: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "ingredients") val ingredients: String?, //MAP in json
    @ColumnInfo(name = "instructions") val instructions: String?,
    @ColumnInfo(name = "dishType") val dishType: Recipe.DishType?,
    @ColumnInfo(name = "difficulty") val difficulty: Recipe.Difficulty?,
    @ColumnInfo(name = "diet") val diet: Recipe.Diet?,
    @ColumnInfo(name = "timeNeeded") val timeNeeded: Int?,
    @ColumnInfo(name = "tags") val tags: String? //arraylist in json
) {

}