package sleet.recipe.providers.recipe

import androidx.room.*

@Dao
interface RecipeDao {
    @Query("SELECT * FROM recipe")
    fun getAll(): List<RecipeEntity>

    @Insert
    fun insert(recipe: RecipeEntity)

    @Delete
    fun delete(recipe: RecipeEntity)

    @Update
    fun update(recipe: RecipeEntity)

}