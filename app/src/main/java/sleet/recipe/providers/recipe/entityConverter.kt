package sleet.recipe.providers.recipe

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import sleet.recipe.domain.Recipe
import java.util.*

fun jsonToMap(value: String): Map<String, String> {
    return Gson().fromJson(value, object : TypeToken<Map<String, Any>>() {}.type)
}

fun jsonToList(value: String): List<String> {
    return Gson().fromJson(value, object : TypeToken<Array<String>>() {}.type)
}

fun toJSon(value: Any): String {
    return Gson().toJson(value)
}

fun Recipe.toRecipeEntity(): RecipeEntity =
   RecipeEntity(
        UUID.randomUUID().toString().toLong(),
        this.name,
        toJSon(this.ingredients),
        this.instructions,
        this.dishType,
        this.difficulty,
        this.diet,
        this.timeNeeded,
        toJSon(this.tags)
   )

