package sleet.recipe.ui.models

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import sleet.recipe.domain.Recipe
import sleet.recipe.domain.RecipeRepo
import javax.inject.Inject

@HiltViewModel
class NewRecipeModel @Inject constructor(val repo: RecipeRepo): ViewModel() {
    fun save(recipe: Recipe) {
        repo.save(recipe)
    }
}