package sleet.recipe.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import sleet.recipe.domain.Recipe
import sleet.recipe.ui.models.NewRecipeModel

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ingredientsForm(recipe: Recipe) {
    Box( modifier = Modifier.border(width = 1.dp, color = Color.Black)
    ) {
        Column {
            Text(text = "ingrédients")
            var ingredientName by remember { mutableStateOf("") }
            var ingredientQuantity by remember { mutableStateOf("") }
            val ingredientFakeMap: MutableMap<String, String> = remember { mutableStateMapOf() }
            val keyboardController = LocalSoftwareKeyboardController.current

            TextField(
                placeholder = {Text("nom")},
                keyboardOptions = KeyboardOptions( imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions( onDone = { keyboardController?.hide() }),
                value = ingredientName,
                onValueChange = { ingredientName = it },
            )
            Row {
                TextField(
                    placeholder = {Text("quantité")},
                    value = ingredientQuantity,
                    keyboardOptions = KeyboardOptions( imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions( onDone = { keyboardController?.hide() }),
                    onValueChange = { ingredientQuantity = it },
                )
                Button(
                    onClick = {
                        if (ingredientName.isNotBlank() && ingredientQuantity.isNotBlank()) {
                            recipe.ingredients[ingredientName] = ingredientQuantity
                            ingredientFakeMap[ingredientName] = ingredientQuantity
                            ingredientName = ""
                            ingredientQuantity = ""
                        }
                    },
                    enabled = true,
                    border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
                    shape = MaterialTheme.shapes.medium,
                ) {
                    Text(text = "ajouter")
                }
            }
            for (ing in ingredientFakeMap) {
                Row {
                    Text(text = "${ing.key}. quantité: ${ing.value}", modifier = Modifier.padding(end = 15.dp))
                    Button(onClick = {
                        recipe.ingredients.remove(ing.key)
                        ingredientFakeMap.remove(ing.key)
                        ingredientName = ""
                        ingredientQuantity = ""
                    }) {
                        Text(text = "retirer")
                    }
                }
            }
        }
    }
}

@Composable
fun saveButton(recipe: Recipe, viewModel: NewRecipeModel) {
    var incompleteForm by remember { mutableStateOf(false) }

    Button(
        onClick = {
            if (recipe.name.isNotEmpty()) {
                viewModel.save(recipe)
            } else {
                incompleteForm = true
            }
        },
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "enregistrer")
        listo
    }
    if (incompleteForm) {
        Text(
            text = "rentrez un nom de recette",
            color = MaterialTheme.colors.error,
            style = MaterialTheme.typography.caption,
            modifier = Modifier.padding(start = 16.dp)
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun tagsForm(recipe: Recipe) {
    Box( modifier = Modifier.border(width = 1.dp, color = Color.Black)
    ) {
        Column {
            Text(text = "tags")
            var tagName by remember { mutableStateOf("") }
            val tagFakeList: MutableList<String> = remember { mutableStateListOf() }
            val keyboardController = LocalSoftwareKeyboardController.current

            Row {
                TextField(
                    value = tagName,
                    onValueChange = { tagName = it },
                    placeholder = { Text("nom du tag") },
                    keyboardOptions = KeyboardOptions( imeAction = ImeAction.Done),
                    keyboardActions = KeyboardActions( onDone = { keyboardController?.hide() }),
                )
                Button(
                    onClick = {
                        if (!tagName.isBlank()) {
                            recipe.tags.add(tagName)
                            tagFakeList.add(tagName)
                            tagName = ""
                        }
                    },
                    enabled = true,
                    border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
                    shape = MaterialTheme.shapes.medium,
                ) {
                    Text(text = "ajouter")
                }
            }
            for (tag in tagFakeList) {
                Row {
                    Text(text = "$tag ", modifier = Modifier.padding(end = 15.dp))
                    Button(onClick = {
                        recipe.tags.remove(tag)
                        tagFakeList.remove(tag)
                        tagName = ""
                    }) {
                        Text(text = "retirer")
                    }
                }
            }
        }
    }
}

@Composable
fun <T> dropDownMenu(recipe: Recipe, placeholder: String, types: Array<T>) {
    var placeholder by remember { mutableStateOf(placeholder) }
    var showMenu by remember { mutableStateOf(false) }
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp),
        elevation = AppBarDefaults.TopAppBarElevation,
        color = MaterialTheme.colors.primary
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Text(text=placeholder)
            Box(modifier = Modifier.weight(1f)) {
                IconButton(onClick = { showMenu = !showMenu }) {
                    Icon(Icons.Default.MoreVert, "")
                }
                DropdownMenu(
                    expanded = showMenu,
                    onDismissRequest = { showMenu = false },
                ) {
                    types.forEach { label ->
                        DropdownMenuItem(onClick = {
                            placeholder = label.toString()
                            addToRecipe(recipe, label)
                            showMenu = false
                        }) {
                            Text(text = label.toString())
                        }
                    }
                }
            }
        }
    }
}

fun <T> addToRecipe(recipe: Recipe, label: T) {
    when(label) {
        is Recipe.DishType -> recipe.dishType= label
        is Recipe.Difficulty -> recipe.difficulty= label
        is Recipe.Diet -> recipe.diet = label
    }
}
