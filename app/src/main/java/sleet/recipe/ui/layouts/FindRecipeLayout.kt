package sleet.recipe.ui.layouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import sleet.recipe.ui.theme.RecipeTheme

class FindRecipeLayout : ComponentActivity() {

    private val types = listOf("nom", "ingredients", "instructions", "dishType","difficulty", "diet", "timeNeeded", "tags")
    private var selectedType by remember { mutableStateOf("") }
    var placeholder by remember { mutableStateOf("type de recherche") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RecipeTheme {
                dropDownMenu()
            }

            if (selectedType.isNotEmpty()) {
                when(selectedType) {
                    "nom" -> searchByNameComponent()
                    "ingredients" -> searchByNameComponent()
                    "instructions" -> searchByNameComponent()
                    "dishType" -> searchByNameComponent()
                    "difficulty" -> searchByNameComponent()
                    "diet" -> searchByNameComponent()
                    "timeNeeded" -> searchByNameComponent()
                    "tags" -> searchByNameComponent()
                }
            }
        }
    }

    @Composable
    fun dropDownMenu() {
        var showMenu by remember { mutableStateOf(false) }
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            elevation = AppBarDefaults.TopAppBarElevation,
            color = MaterialTheme.colors.primary
        ) {
            Row( modifier = Modifier.fillMaxWidth() ) {

                Text( text = placeholder)

                Box( modifier = Modifier.weight(1f) ) {

                    IconButton(onClick = { showMenu = !showMenu }) {
                        Icon(Icons.Default.MoreVert, "")
                    }

                    DropdownMenu(
                        expanded = showMenu,
                        onDismissRequest = { showMenu = false },
                    ) {
                        types.forEach { label ->
                            DropdownMenuItem(onClick = {
                                placeholder = label
                                selectedType = label
                                showMenu = false
                            }) {
                                Text(text = label)
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun searchByNameComponent() {

    }
}