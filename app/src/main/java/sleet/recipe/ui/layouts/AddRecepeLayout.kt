package sleet.recipe.ui.layouts

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import sleet.recipe.MainMenu
import sleet.recipe.domain.Recipe
import sleet.recipe.ui.composables.dropDownMenu
import sleet.recipe.ui.composables.ingredientsForm
import sleet.recipe.ui.composables.saveButton
import sleet.recipe.ui.composables.tagsForm
import sleet.recipe.ui.models.NewRecipeModel
import sleet.recipe.ui.theme.RecipeTheme

@AndroidEntryPoint
class AddRecipeLayout : ComponentActivity() {

	lateinit var viewModel: NewRecipeModel
	private val recipe = Recipe()
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = ViewModelProvider(this)[NewRecipeModel::class.java]
		setContent {
			RecipeTheme {
				val context = LocalContext.current
				Surface(color = MaterialTheme.colors.background) {
					Column(
						modifier = Modifier
                            .verticalScroll(rememberScrollState())
                            .fillMaxWidth(),
						verticalArrangement = Arrangement.Center
					) {

						newRecipeForm(recipe)

						Button(
							onClick = {
								context.startActivity((Intent(context, MainMenu::class.java)))
							},
							enabled = true,
							border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
							shape = MaterialTheme.shapes.medium,
						) {
							Text(text = "retour")
						}
					}
				}
			}
		}
	}

	@OptIn(ExperimentalComposeUiApi::class)
	@Composable
	fun newRecipeForm(recipe: Recipe) {
		val keyboardController = LocalSoftwareKeyboardController.current
		var recipeName by remember { mutableStateOf("") }
		var instructions by remember { mutableStateOf("") }
		var timeNeeded by remember { mutableStateOf("") }

		TextField(
			value = recipeName,
			label = { Text(text = "nom") },
			placeholder = { Text("nouvelle recette") },
			keyboardOptions = KeyboardOptions( imeAction = ImeAction.Done),
			keyboardActions = KeyboardActions( onDone = { keyboardController?.hide() }),
			onValueChange = {
				recipeName = it
				recipe.name = it
			},

		)

		ingredientsForm(recipe)

		TextField(
			value = instructions,
			onValueChange = {
				recipe.instructions = it
				instructions = it
			},
			placeholder = { Text("ecrivez ici vos instructions") },
		)

		dropDownMenu(recipe, "type de recette", enumValues<Recipe.DishType>())

		dropDownMenu(recipe, "difficulté", enumValues<Recipe.Difficulty>())

		dropDownMenu(recipe, "régime", enumValues<Recipe.Diet>())

		tagsForm(recipe)

		TextField(
			value = timeNeeded,
			modifier = Modifier.fillMaxWidth(),
			keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
			label = { Text(text = "difficulté (en minute)") },
			placeholder = { Text("Ex: 30") },
			onValueChange = {
				 recipe.timeNeeded = it.toInt()
				timeNeeded = it
			}
		)

		saveButton(recipe, viewModel)
	}
}

