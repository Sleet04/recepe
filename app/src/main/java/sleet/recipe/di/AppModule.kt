package sleet.recipe.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import sleet.recipe.domain.AppDatabase
import sleet.recipe.providers.recipe.RecipeDao
import sleet.recipe.ui.models.NewRecipeModel
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

	@Provides
	@Singleton
	fun providesDatabase(@ApplicationContext context: Context): AppDatabase {
		return Room.databaseBuilder(context, AppDatabase::class.java, "recipe_database")
			.allowMainThreadQueries()
			.fallbackToDestructiveMigration()
			.build()
	}

	@Provides
	@Singleton
	fun provideRecipeDao(recipeDatabase: AppDatabase): RecipeDao {
		return recipeDatabase.recipeDao()
	}

}