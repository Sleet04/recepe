package sleet.recipe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sleet.recipe.ui.layouts.AddRecipeLayout
import sleet.recipe.ui.layouts.FindRecipeLayout
import sleet.recipe.ui.theme.RecipeTheme

class MainMenu : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RecipeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Column (
                        modifier = Modifier.fillMaxWidth(),
                        verticalArrangement = Arrangement.Center
                    ){
                        recipeListButton()
                        newRecipeButton()
                        findRecipeButton()
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun recipeListButton() {
    Button(
        onClick = {},
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "liste des recettes")
    }
}

//@Preview(showBackground = true)
@Composable
fun newRecipeButton() {
    val context = LocalContext.current
    Button(
        onClick = {
            startAddRecipe(context)
                  },
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "nouvelle recette")
    }
}

@Composable
fun findRecipeButton() {
    val context = LocalContext.current
    Button(
        onClick = {
            startFindRecipe(context)
        },
        enabled = true,
        border = BorderStroke(width = 1.dp, brush = SolidColor(Color.Blue)),
        shape = MaterialTheme.shapes.medium,
    ) {
        Text(text = "rechercher une recette")
    }
}

fun startAddRecipe(context: Context) {
    context.startActivity((Intent(context , AddRecipeLayout::class.java)))
}

fun startFindRecipe(context: Context) {
    context.startActivity((Intent(context , FindRecipeLayout::class.java)))
}

@Preview(showBackground = true)
@Composable
fun searchBar() {
    var text by remember { mutableStateOf("") }

    TextField(
        value = text,
        onValueChange = { text = it },
        label = { Text("Recette") },
        placeholder = { Text("rechercher un recette") },
    )
}
